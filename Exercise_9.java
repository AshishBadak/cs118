import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_9
{
    int randNum;
    int direction;
    int wallsCount;
    String heading;
    int stepCount  = 0;
    int maxStepsInOneDir;

    public void controlRobot(IRobot robot)
    {
    	if(stepCount == 0)
    		maxStepsInOneDir = randomNumber(1, 8);	//sets allowed moves in one direction between 1 to 8.

    	stepCount++;
    	heading = "forward";
    	direction = IRobot.AHEAD;
	
	/*	Change direction
			(i)  if there is wall ahead
			OR
			(ii) randomly between 1 to 8 moves
    		else move ahead in same direction:
    	*/
	while( ( robot.look(direction) == IRobot.WALL) || (stepCount == maxStepsInOneDir) )
	{
		stepCount = 0;

		/*
		Select a random number with association with Math.floor() instead of Math.round()
		Now probability for all cases is equal to : 1/4.
		*/
		randNum = randomNumber(0, 3);

		// Convert this to a direction
	    	switch(randNum)
	    	{
	    		case 0:	direction = IRobot.LEFT;
	    			heading = "left";
	    			break;
	    		case 1:	direction = IRobot.RIGHT;
	    			heading = "right";
	    			break;
			case 2:	direction = IRobot.BEHIND;
	    			heading = "backwards";
				break;
			case 3:	direction = IRobot.AHEAD;
	    			heading = "forward";
	    	}
     	}
	
	 // Face the robot in this direction 
	robot.face(direction);
	
	// Detect the current location of robot is deadend, junction, cross-raod or corridor and print log.
	detectLocation(robot);
	System.out.println("I am going "+heading);
	
	//and move the robot
	robot.advance();
    }

    private void detectLocation(IRobot robot)
    {
    	wallsCount = 0;

    	// Loop checks no. of surrounding walls to the robot
    	for (int side = 0; side<4; side++)
    	{
    		if(robot.look(robot.AHEAD+side) == IRobot.WALL)
    		{
    			wallsCount++;
    		}
    	}

    	// Decide the type of location
    	switch(wallsCount)
	{
		case 0:	heading += " at cross-roads.";
	    		break;
	    	case 1:	heading += " at a junction.";
	    		break;
		case 2:	heading += " down a corridor.";
			break;
		default:	heading += " at a deadend.";
	}
    }

    // Generates random number within range min-max (boundaries inclusive)
    private int randomNumber(int min, int max)
    {
    	return (int) Math.floor( Math.random() * (max - min + 1) + min);
    }

}
