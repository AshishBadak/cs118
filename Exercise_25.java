import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_25
{
	private int pollRun = 0;			// Counter for no. of steps in each run
	private RobotData23 robotData;
	private int explorerMode;		// 1 = explorer mode; 0 = backtracking mode
	private boolean firstVisit;

	public void controlRobot(IRobot robot)
	{
		int direction;

		if ( (robot.getRuns() == 0) && (pollRun == 0))
		{
			// On the first move of the first run of a new maze
			robotData = new RobotData23();
			explorerMode = 1;
			firstVisit = false;
		}

		if (explorerMode == 1)				// Unecountered pathway available
			direction = exploreControl(robot);
		else						// No unencountered pathway available; backtrack
			direction = backtrackControl(robot);
		
		if(direction != IRobot.CENTRE)			// If maze is not fully explored
		{
			/*
			If robot is going to visit the location for the first time --> set 'firstVisit' to 'true'
			else robot is re-visiting that locaation --> set 'firstVisit' to 'false'
			*/
			if(robot.look(direction) == IRobot.PASSAGE)
				firstVisit = true;	
			else
				firstVisit = false;
			
			robot.face(direction);  			// Face the robot in this direction
			robot.advance();			// Move the robot
		}
		pollRun++;					// Increment for each step
	}

	// Controller for exploring mode
	public int exploreControl(IRobot robot)
	{
		int direction;
		int exits = nonwallExits(robot);
		int beenbeforeExits = beenbeforeExits(robot);

		// Set the 'direction' based on exits available at current position
		switch(exits)
		{
			case 1 : // Only 1 exit; so ROBOT is at DEAD-END
				direction = deadEnd(robot);
				break;
			case 2 : // 2 exits; so ROBOT is at CORRIDOR
				direction = corridor(robot);
				break;
			default : // 3 or 4 exits; so ROBOT is at JUNCTION or CROSS-ROAD
				if ( (firstVisit == true) || (beenbeforeExits < 2))			// If it is the starting point or new junction
				{
					direction = junctionOrCrossRoad(robot);
				}
				else
				{
					direction = deadEnd(robot);				// need to turn back; loop encountered
				}
		}
		return direction;
	}

	// Controller for backtracking mode
	public int backtrackControl(IRobot robot)
	{
		int direction;
		int exits = nonwallExits(robot);
		int passageExits = passageExits(robot);

		switch(exits)
		{
			case 1 : // Only 1 exit; so ROBOT is at DEAD-END
				direction = deadEnd(robot);
				break;
			case 2 : // 2 exits; so ROBOT is at CORRIDOR
				if (passageExits != 0)
					explorerMode =1;		// Unvisited passage found; change mode to explore
				direction = corridor(robot);
				break;
			default : // 3 or 4 exits; so ROBOT is at JUNCTION or CROSS-ROAD
				if (passageExits != 0)
					explorerMode = 1;	// no new pathways; select backtrack mode
				direction = junctionOrCrossRoad(robot);	
		}
		return direction;
	}

	// Returns count of available exits at robot's current position
	private int nonwallExits(IRobot robot)
	{
	   	int nonWallExitsCount = 0;

    		// Loop checks no. of non-wall sides to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
    			{
	    			nonWallExitsCount++;
    			}
    		}
    		return nonWallExitsCount;
	}

	// Returns count of non-visited exits at robot's current position
	private int passageExits(IRobot robot)
	{
	   	int passageExitsCount = 0;

    		// Loop checks no. of PASSAGE adjacent to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
    			{
	    			passageExitsCount++;
    			}
    		}
    		return passageExitsCount;
	}

	// Returns count of visits to current position
	private int beenbeforeExits(IRobot robot)
	{
	   	int beenbeforeExitsCount = 0;

    		// Loop checks no. of visited exits adjacent to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) == IRobot.BEENBEFORE)
    			{
	    			beenbeforeExitsCount++;
    			}
    		}
    		return beenbeforeExitsCount;
	}

	// Handles the situation when robot is at a dead-end
	private int deadEnd(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;

		if (pollRun == 0)
		{	
			/*
			Dont reverse direction as robot is at the starting position;
			instead choose only available exit
			*/
			for(int i = 1; i<=3 && ( robot.look(optimalDirection)==IRobot.WALL) ; i++)
			{
				optimalDirection = IRobot.AHEAD+i;
			}
		}
		else
		{	// Turn back;
			explorerMode = 0;
			optimalDirection = IRobot.BEHIND;
		}
		return optimalDirection;
	}

	// Handles the situation when robot is down a corridor
	private int corridor(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;
		
		// Finds exit other than the one robot is coming from
		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
		{
			// Road available; Go AHEAD
			optimalDirection = IRobot.AHEAD;
		}
		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
		{
			// Road available; Turn LEFT
			optimalDirection = IRobot.LEFT;
		}
		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
		{
			// Road available; Turn RIGHT
			optimalDirection = IRobot.RIGHT;
		}
		return optimalDirection;
	}

	// Handles the situation when robot is at a junction or cross-road
	private int junctionOrCrossRoad(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;
		int randomNum;
		
		if (firstVisit)	// If Junction is visited first-time, save heading
		{
			robotData.recordJunction(robot.getHeading());
		}

		if (explorerMode == 0)
		{
			// Go towards where robot came from at first time
	     		int arrivedFrom = robotData.searchJunction();
			int goTowards;

			// Reverse arrival direction
			switch(arrivedFrom)
			{
				case IRobot.NORTH :
					goTowards = IRobot.SOUTH ;
					break;
				case IRobot.EAST :
					goTowards = IRobot.WEST;
					break;
				case IRobot.SOUTH :
					goTowards = IRobot.NORTH;
					break;
				case IRobot.WEST :
					goTowards = IRobot.EAST;
					break;
				default :	// All junctions fully visited;
					optimalDirection = IRobot.CENTRE;
					goTowards = IRobot.EAST;
			}

			// Set robot's face opposite to what it was at initial arrival time
			robot.setHeading(goTowards);
		}
		else
		{	// One or more PASSAGE available; Select randomly among them
			do
			{
				randomNum = randomNumber(0, 3);
				optimalDirection = IRobot.AHEAD+randomNum;
			}while(robot.look(optimalDirection) != IRobot.PASSAGE);
		}
		
		return optimalDirection;
	}

	/** Invoked when Reset button is pressed
	**  Resets utility variables after each run
	*/
	public void reset()
	{
		pollRun = 0;
		explorerMode = 1;
		firstVisit = false;
		robotData.resetJunctionCounter();
	}

	// Returns a random number within range of min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}

/**
* This class stores log of visited junctions
*/
class RobotData23
{
	private static int junctionCounter;		// no. of junctions visited
	private static int maxJunctions = 1000;
	private int[] arrived;				// direction from which robot arrived to junction

	public RobotData23()
	{
		// Initialize variables
		junctionCounter = 0;
		arrived = new int [maxJunctions];
	}

	// Resets visited junction counter to 0 after each run
	public void resetJunctionCounter()
	{
		junctionCounter = 0;
	}

	// Saves log of unencountered junction robot is visiting
	public void recordJunction(int heading)
	{
		arrived [junctionCounter++] = heading;
	}

	public int searchJunction()
	{
		if(junctionCounter <= 0)			// If all junctions completely visited
			return -1;				// return -1
		else						// else
			return arrived[--junctionCounter];	// return recently encountered junction's arrived direction
	}
}