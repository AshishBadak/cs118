import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_14
{
	private int randNum;
	private int direction;
	private byte facingV;
	private byte facingH;

	public void controlRobot(IRobot robot)
	{
		facingV = isTargetNorth(robot);
		facingH = isTargetEast(robot);

		if( robot.getHeading() == IRobot.NORTH )
		{
			northController(robot);
		}

		// Face the robot in this direction
		robot.face(direction);  

		// Move the robot
		robot.advance();
	}

	// Generates random number within range min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}

	private void northController(IRobot robot)
	{
		do
		{
			//Select a random number with equal probability.
			randNum = randomNumber(0, 1);
			
			// Convert this to a direction
			switch(randNum)
			{
				case 0: 
					// Turn LEFT or RIGHT w.r.t. Target Location
					
					if(facingH == 1)	// target is in the east
						direction = IRobot.RIGHT;
					else if(facingH == -1)	// target is in the west
						direction = IRobot.LEFT;
					else			// target is at same latitude
						direction = IRobot.AHEAD + Math.abs( facingV -1 );
					break;

				default:	 
					// Move AHEAD or BEHIND w.r.t. Target Location
				
					if(facingV == 1)	// target is in the north
						direction = IRobot.AHEAD;
					else if(facingV == -1)	// target is in the south
						direction = IRobot.BEHIND;
					else			// target is at same longitude
						direction = IRobot.BEHIND - facingH;
			}
		}while(robot.look(direction)==IRobot.WALL);
	}

	// Checks whether Target is in the North direction or in the South relative to Robot's current position
	private byte isTargetNorth(IRobot robot)
	{
		byte result;

		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same latitude’

		if (robot.getLocationY() > robot.getTargetLocation().y )
		{
			result = 1;
		}
		else if (robot.getLocationY() < robot.getTargetLocation().y )
		{
			result = -1;
		}
		else
		{
			result = 0;
		}
		return result;
	}

	// Checks whether Target is in the East direction or in the West relative to Robot's current position
	private byte isTargetEast(IRobot robot)
	{
		byte result;

		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same longitude'

		if (robot.getLocationX() < robot.getTargetLocation().x )
		{
			result = 1;
		}
		else if (robot.getLocationX() > robot.getTargetLocation().x )
		{
			result = -1;
		}
		else
		{
			result = 0;
		}
		return result;
	}
}
