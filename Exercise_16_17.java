import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_16_17
{
	private int randNum;
	private int direction;
	private byte facingV;
	private byte facingH;
	private int offsetToTurnLR;
	private int offsetToMoveFB;

	public void controlRobot(IRobot robot)
	{

		facingV = isTargetNorth(robot);
		facingH = isTargetEast(robot);

		// Select appropriate controller based on robot's heading :
		if( robot.getHeading() == IRobot.NORTH )
			northController(robot);
		if( robot.getHeading() == IRobot.SOUTH )
			southController(robot);
		if( robot.getHeading() == IRobot.EAST )
			eastController(robot);
		if( robot.getHeading() == IRobot.WEST )
			westController(robot);

		robotTurner(robot);

		// Face the robot in this direction
		robot.face(direction);  

		// Move the robot
		robot.advance();
	}

	// Generates random number within range min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}

	// If ROBOT is heading towards NORTH then this controller should be used to set offsets:
	private void northController(IRobot robot)
	{
		offsetToTurnLR = -facingH;
		offsetToMoveFB = facingV;
	}

	// If ROBOT is heading towards SOUTH then this controller should be used to set offsets:
	private void southController(IRobot robot)
	{
		offsetToTurnLR = facingH;
		offsetToMoveFB = -facingV;
	}

	// If ROBOT is heading towards EAST then this controller should be used to set offsets:
	private void eastController(IRobot robot)
	{
		offsetToTurnLR = facingV;
		offsetToMoveFB = facingH;
	}

	// If ROBOT is heading towards WEST then this controller should be used to set offsets:
	private void westController(IRobot robot)
	{
		offsetToTurnLR = -facingV;
		offsetToMoveFB = -facingH;
	}

	// Turn or Move the ROBOT using offsets set in controller functions:
	private void robotTurner(IRobot robot)
	{
		do
		{
			//Select a random number with equal probability.
			randNum = randomNumber(0, 1);
			
			// Convert this to a direction
			switch(randNum)
			{
				case 0: 	// Turn
					if (offsetToTurnLR == 0)
					{	// Turning not feasible as target is at same latitude;
						// Go AHEAD or BEHIND w.r.t. Target Location
						direction = IRobot.AHEAD + Math.abs( offsetToMoveFB -1 );
					}
					else
					{
						// Turn LEFT or RIGHT w.r.t. Target Location
						direction = IRobot.AHEAD + (offsetToTurnLR+2);
					}
					break;

				default:	//Move
					if(offsetToMoveFB == 0)
					{
						// Moving back and forth not feasible as target is at same longitude;
						// Turn LEFT or RIGHT w.r.t. Target Location
						direction = IRobot.AHEAD + (offsetToTurnLR+2);
					}
					else
					{
						// Move AHEAD or BEHIND w.r.t. Target Location
						direction = IRobot.AHEAD + Math.abs( offsetToMoveFB -1 );
					}
			}
		}while(robot.look(direction)==IRobot.WALL);
	}

	// Checks whether Target is in the North direction or in the South relative to Robot's current position
	private byte isTargetNorth(IRobot robot)
	{
		byte result;

		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same latitude’

		if (robot.getLocationY() > robot.getTargetLocation().y )
			result = 1;
		else if (robot.getLocationY() < robot.getTargetLocation().y )
			result = -1;
		else
			result = 0;
		return result;
	}

	// Checks whether Target is in the East direction or in the West relative to Robot's current position
	private byte isTargetEast(IRobot robot)
	{
		byte result;

		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same longitude'

		if (robot.getLocationX() < robot.getTargetLocation().x )
			result = 1;
		else if (robot.getLocationX() > robot.getTargetLocation().x )
			result = -1;
		else
			result = 0;
		return result;
	}
}