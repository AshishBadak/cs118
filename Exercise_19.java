import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_19
{
	private int pollRun = 0;

	public void controlRobot(IRobot robot)
	{
		int direction;
		int exits = nonwallExits(robot);

		// Set the 'direction' based on exits available at current position
		switch(exits)
		{
			case 1 : // Only 1 exit; so ROBOT is at DEAD-END
				direction = deadEnd(robot);
				break;
			case 2 : // 2 exits; so ROBOT is at CORRIDOR
				direction = corridor(robot);
				break;
			default : // 3 or 4 exits; so ROBOT is at JUNCTION or CROSS-ROAD
				direction = junctionOrCrossRoad(robot);
				break;
		}

		// Face the robot in this direction
		robot.face(direction);  

		// Move the robot
		robot.advance();

		pollRun++;
	}

	// Returns count of available exits at robot's current position
	private int nonwallExits(IRobot robot)
	{
	   	int nonWallExitsCount = 0;

    		// Loop checks no. of non-wall sides to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
    			{
	    			nonWallExitsCount++;
    			}
    		}

    		return nonWallExitsCount;
	}

	// Returns count of non-visited exits at robot's current position
	private int passageExits(IRobot robot)
	{
	   	int passageExitsCount = 0;

    		// Loop checks no. of PASSAGE adjacent to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
    			{
	    			passageExitsCount++;
    			}
    		}
    		return passageExitsCount;
	}

	// Handles the situation when robot is at a dead-end
	private int deadEnd(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;

		if (pollRun == 0)
		{	
			/*
			Dont reverse direction as we are at starting position;
			instead chooseonly available exit
			*/
			for(int i = 1; i<=3 && ( robot.look(optimalDirection)==IRobot.WALL) ; i++)
			{
				optimalDirection = IRobot.AHEAD+i;
			}
		}
		else
		{	// Turn back;
			optimalDirection = IRobot.BEHIND;
		}
		return optimalDirection;
	}

	// Handles the situation when robot is down a corridor
	private int corridor(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;
		
		// Finds exit other than the one robot is coming from
		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
		{
			// Road available; Go AHEAD
			optimalDirection = IRobot.AHEAD;
		}
		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
		{
			// Road available; Turn LEFT
			optimalDirection = IRobot.LEFT;
		}
		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
		{
			// Road available; Turn RIGHT
			optimalDirection = IRobot.RIGHT;
		}
		return optimalDirection;
	}

	// Handles the situation when robot is at a junction or cross-road
	private int junctionOrCrossRoad(IRobot robot)
	{
		int optimalDirection;
		int passageExits = passageExits(robot);
		int randomNum;

		if (passageExits == 0)
		{
			// No non-visited branches; select randomly from available exits
			
			do
			{	//keeps choosing a direction till it finds a non wall one except behind
				randomNum = randomNumber(1,3);
				switch(randomNum)
				{
					case 1: optimalDirection = IRobot.RIGHT;
						break;
					case 2: optimalDirection = IRobot.LEFT;
						break;
					default:optimalDirection = IRobot.AHEAD;
						break;
				}
	     		}while( (robot.look(optimalDirection)==IRobot.WALL) );
		}
		else
		{
			// One or more PASSAGE available; Select randomly among them
			do
			{
				randomNum = randomNumber(0, 3);
				optimalDirection = IRobot.AHEAD+randomNum;
			}while(robot.look(optimalDirection) != IRobot.PASSAGE); // keeps choosing a direction till it finds a passage
		}
		
		return optimalDirection;
	}

	// Invoked when Reset button is pressed
	public void reset()
	{
		pollRun = 0;
	}

	// Returns a random number within range of min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}