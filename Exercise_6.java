import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_6
{
    int randNum;
    int direction;
    //int wallsCount;
    //String heading;
    //int turnCount[] = new int [4];

    	int turnCount[] = {0,0,0,0};
    public void controlRobot(IRobot robot)
    {
    	//heading = "";
	System.out.print("I am going ");
	do
	{
		//Select a random number
		randNum = (int) Math.round(Math.random()*3);

		// Convert this to a direction
	    	switch(randNum)
	    	{
	    		case 1:	direction = IRobot.RIGHT;
	    			//heading = "right";
	    			break;
			case 2:	direction = IRobot.BEHIND;
	    			//heading = "backwards";
				break;
	    		case 3:	direction = IRobot.LEFT;
	    			//heading = "left";
	    			break;
			default:	direction = IRobot.AHEAD;
	    			//heading = "forward";
	    	}
     	}while(robot.look(direction)==IRobot.WALL);//keeps choosing a direction till it finds a non wall one
	
	 // Face the robot in this direction 
	robot.face(direction);
	
	// Detect the current location of robot is deadend, junction, cross-raod or corridor and print it.
	//detectLocation(robot);
	//System.out.println(heading);
	
	// Print turn count :	
	turnCount[(direction%2000)]++;
	System.out.println("AHEAD : "+turnCount[0]+". RIGHT : "+turnCount[1]+". BEHIND : "+turnCount[2]+". LEFT : "+turnCount[3]);

	//and move the robot
	robot.advance();
    }
/*
    private void detectLocation(IRobot robot)
    {
    	wallsCount = 0;

    	// Loop checks no. of surrounding walls to the robot
    	for (int side = 0; side<4; side++)
    	{
    		if(robot.look(robot.AHEAD+side) == IRobot.WALL)
    		{
    			wallsCount++;
    		}
    	}

    	// Decide the type of location
    	switch(wallsCount)
	{
		case 0:	heading += " at cross-roads.";
	    		break;
	    	case 1:	heading += " at a junction.";
	    		break;
		case 2:	heading += " down a corridor.";
			break;
		default:	heading += " at a deadend.";
	}
    }
*/
}
