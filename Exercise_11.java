import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_11
{

	private int randNum;
	private int direction;

	public void controlRobot(IRobot robot)
	{
		do
		{
			//Select a random number with equal probability
			randNum = randomNumber(0, 3);
			
			// Convert this to a direction
			switch(randNum)
			{
				case 0:	direction = IRobot.LEFT;
					break;
				case 1:	direction = IRobot.RIGHT;
					break;
				case 2:	direction = IRobot.BEHIND;
					break;
				default:	direction = IRobot.AHEAD;
			}
		}while(robot.look(direction)==IRobot.WALL);//keeps choosing a direction till it finds a non wall one

		// Face the robot in this direction
		robot.face(direction);  

		// Move the robot
		robot.advance();
	}

	// Generates random number within range min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( ( Math.random() * (max - min + 1) ) + min);
	}
}
