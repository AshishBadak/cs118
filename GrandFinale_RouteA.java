import uk.ac.warwick.dcs.maze.logic.IRobot;

public class GrandFinale_RouteA
{
	private int pollRun = 0;		// Counter for no. of steps in each run
	private RobotData_RouteA robotData;
	private int explorerMode;	// 1 = explorer mode; 0 = backtracking mode
	private int learning;		// 1 = learn; 0 = apply learned knowledge

	public void controlRobot(IRobot robot)
	{
		int direction = IRobot.CENTRE;

		/*
		If --> First run of new maze --> Learn
		Else --> Repeated run --> Apply learned knowledge
		*/
		if ( robot.getRuns() == 0)
		{
			learning = 1;
			if (pollRun == 0)
			{
				// On the first move of the first run of a new maze
				robotData = new RobotData_RouteA();
				explorerMode = 1;				
			}

			if (explorerMode == 1)				// Unecountered pathway available; explore
				direction = exploreControl(robot);
			else						// No unencountered pathway available; backtrack
				direction = backtrackControl(robot);
		}
		else
		{
			learning = 0;
			direction = smartControl(robot);
		}
	
		robot.face(direction);  		// Face the robot in this direction

		/*
		When robot is learning and positioned at junction update knowlwdge of that junction
		Pass value -> 1 for selected direction
		*/
		if ( (nonwallExits(robot) > 2) && (learning == 1) )
			updateKnowledge(robot.getLocationX(), robot.getLocationY(), robot.getHeading(), 1);

		robot.advance();		// Move the robot
		pollRun++;			// Increment for each step
	}

	// Controller for exploring mode
	public int exploreControl(IRobot robot)
	{
		int direction;
		int exits = nonwallExits(robot);

		// Set the 'direction' based on exits available at current position
		switch(exits)
		{
			case 1 : // Only 1 exit; so ROBOT is at DEAD-END
				direction = deadEnd(robot);
				break;
			case 2 : // 2 exits; so ROBOT is at CORRIDOR
				direction = corridor(robot);
				break;
			default : // 3 or 4 exits; so ROBOT is at JUNCTION or CROSS-ROAD
				direction = junctionOrCrossRoad(robot);
				break;
		}
		return direction;
	}

	// Controller for backtracking mode
	public int backtrackControl(IRobot robot)
	{
		int direction;
		int exits = nonwallExits(robot);
		switch(exits)
		{
			case 1 : // Only 1 exit; so ROBOT is at DEAD-END
				direction = deadEnd(robot);
				break;
			case 2 : // 2 exits; so ROBOT is at CORRIDOR
				if (passageExits(robot) != 0)
					explorerMode =1;		// Unvisited passage found; change mode to explore
				direction = corridor(robot);
				break;
			default : // 3 or 4 exits; so ROBOT is at JUNCTION or CROSS-ROAD
				direction = junctionOrCrossRoad(robot);
		}
		return direction;
	}

	// Controller for repeated run of a maze
	public int smartControl(IRobot robot)
	{
		int direction;
		int exits = nonwallExits(robot);
		switch(exits)
		{
			case 1 : // Only 1 exit; so ROBOT is at DEAD-END
				direction = deadEnd(robot);
				break;
			case 2 : // 2 exits; so ROBOT is at CORRIDOR
				direction = corridor(robot);
				break;
			default : // 3 or 4 exits; so ROBOT is at JUNCTION or CROSS-ROAD
				direction = applyGainedKnowledge(robot);
		}
		return direction;
	}

	// Uses saved junction log for ruturning direction which leads to the target
	public int applyGainedKnowledge(IRobot robot)
	{
		int direction;

		// Get the turn which leads to goal
		int turn = robotData.getOptimalTurn(robot.getLocationX(), robot.getLocationY());
		
		if (turn == -1)					// Target cant be reached here onwards; turn behind
			direction = IRobot.BEHIND;
		else						// Set direction which leads to the target
			direction = IRobot.AHEAD + turn;

		return direction;
	}

	// Returns count of available exits at robot's current position
	private int nonwallExits(IRobot robot)
	{
	   	int nonWallExitsCount = 0;

    		// Loop checks no. of non-wall sides to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
    			{
	    			nonWallExitsCount++;
    			}
    		}

    		return nonWallExitsCount;
	}

	// Returns count of non-visited exits at robot's current position
	private int passageExits(IRobot robot)
	{
	   	int passageExitsCount = 0;

    		// Loop checks no. of PASSAGE adjacent to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
    			{
	    			passageExitsCount++;
    			}
    		}
    		return passageExitsCount;
	}

	// Returns count of visits to current position
	private int beenbeforeExits(IRobot robot)
	{
	   	int beenbeforeExitsCount = 0;

    		// Loop checks no. of visited exits adjacent to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) == IRobot.BEENBEFORE)
    			{
	    			beenbeforeExitsCount++;
    			}
    		}
    		return beenbeforeExitsCount;
	}

	/*
	Looks around the robot and saves the log of surrounding conditions
	*/
	public int[] gainKnowledge(IRobot robot)
	{
		int[] surroundingConditions = new int[4];

		// Look around and save exits' status
		for (int side = 0; side<4; side++)
    		{
    			switch(robot.look(IRobot.AHEAD+side))
    			{
    				case IRobot.BEENBEFORE :
    					surroundingConditions[side] = 0;
    					break;
    				case IRobot.PASSAGE :
    					surroundingConditions[side] = -1;
    					break;
    				case IRobot.WALL :
    					surroundingConditions[side] = -100;
    					break;
    			}
    		}
    		return surroundingConditions;
	}

	// Returns the reverse facing of that of robot
	public int reverseFacing(int facing)
	{
		int reverseFacing;

		// Reverse robot facing
		switch(facing)
		{
			case IRobot.NORTH :
				reverseFacing = IRobot.SOUTH ;
				break;
			case IRobot.EAST :
				reverseFacing = IRobot.WEST;
				break;
			case IRobot.SOUTH :
				reverseFacing = IRobot.NORTH;
				break;
			default :
				reverseFacing = IRobot.EAST;
		}
		return reverseFacing;
	}

	/*
	Update knowledge of already saved junctionaccording to current situation
	accepts value to be updated (0 --> for wrong and visited path; 1--> for path currently selected)
	*/
	public void updateKnowledge(int xPos, int yPos, int currentHeading, int value)
	{
		int index = 0;

		// Get intial heading
		int arrivedFrom = robotData.searchJunction(xPos, yPos);

		// If value == 0 --> reverse facing for index adjustment
		if(value == 0)
			currentHeading = reverseFacing(currentHeading);

		// Adjust index to look around according to initial heading
		switch(arrivedFrom)
		{
			case IRobot.NORTH :
				index = 0;
				break;
			case IRobot.EAST :
				index = -1;
				break;
			case IRobot.SOUTH :
				index = -2;
				break;
			case IRobot.WEST :
				index = -3;
				break;
		}

		// Look around all directions and select one that needs to be udated with corresponding value(0 or 1)
		for(int i = index; i<(4+index); i++)
		{
			if( (arrivedFrom+i) == currentHeading )
				robotData.updateJunction(xPos, yPos, ((4+i)%4), value);
		}
	}

	// Handles the situation when robot is at a dead-end
	private int deadEnd(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;

		if (pollRun == 0)
		{	
			/*
			Dont reverse direction as we are at starting position;
			instead choose only available exit
			*/
			for(int i = 1; i<=3 && ( robot.look(optimalDirection)==IRobot.WALL) ; i++)
			{
				optimalDirection = IRobot.AHEAD+i;
			}
		}
		else
		{	// Turn back;
			explorerMode = 0;
			optimalDirection = IRobot.BEHIND;
		}
		return optimalDirection;
	}

	// Handles the situation when robot is down a corridor
	private int corridor(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;
		
		// Finds exit other than the one robot is coming from
		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
		{
			// Road available; Go AHEAD
			optimalDirection = IRobot.AHEAD;
		}
		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
		{
			// Road available; Turn LEFT
			optimalDirection = IRobot.LEFT;
		}
		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
		{
			// Road available; Turn RIGHT
			optimalDirection = IRobot.RIGHT;
		}
		return optimalDirection;
	}

	// Handles the situation when robot is at a junction or cross-road
	private int junctionOrCrossRoad(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;
		int passageExits = passageExits(robot);			// No. of unvisited exits at current junction
		int visitCount  = beenbeforeExits(robot);		// No. of times robot visited current junction
	
		// Get robot log ---> x-y co-ordinate and heading
		int xPos = robot.getLocationX();
		int yPos = robot.getLocationY();
		int currentHeading = robot.getHeading();

		if (visitCount == 1)					
		{
			// If Junction is visited first-time; save the log of that junction
			int[] surroundingConditions = gainKnowledge(robot);
			robotData.recordJunction(xPos, yPos, currentHeading, surroundingConditions);
		}
		else if (visitCount > 1)
		{
			/* If robot is re-visiting current junction (i.e. selected path was wrong)
			    Update knowledge base of current junction for that direction
			    by setting value to 0 (i.e. wrong way / visited)
			*/
			updateKnowledge(xPos, yPos, currentHeading, 0);
		}
		
		if (passageExits == 0)
			explorerMode = 0;	// no new pathways; select backtrack mode
		else
			explorerMode = 1;	// Unvisited passage found; change mode to explore

		if (explorerMode == 0)
		{
			// Backtrack -- > Go towards where robot came from at first time
	     		int arrivedFrom = robotData.searchJunction(xPos, yPos);
			int goTowards = reverseFacing(arrivedFrom);
			robot.setHeading(goTowards);
		}
		else
		{
			// One or more PASSAGE available; Select randomly among them
			int randomNum;
			do
			{
				randomNum = randomNumber(0, 3);
				optimalDirection = IRobot.AHEAD+randomNum;
			}while(robot.look(optimalDirection) != IRobot.PASSAGE);
		}
		
		return optimalDirection;
	}

	// Invoked when Reset button is pressed
	public void reset()
	{
		pollRun = 0;
		explorerMode = 1;
	}

	// Returns a random number within range of min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}


/**
* This class stores log of visited junctions
*/
class RobotData_RouteA
{
	private static int maxJunctions = 10000;
	private static int junctionCounter;		// no. of junctions visited
	private static JunctionData junctionData[];

	// This inner-class saves junction-specific data
	private class JunctionData
	{
		private int juncX;				// x co-ordinate of junction
		private int juncY;				// y co-ordinate of junction
		private int arrived;				// direction from which robot arrived to junction
		private int[] surroundingConditions;

		public JunctionData(int xPos, int yPos, int heading, int[] surroundingConditions)
		{
			juncX = xPos;
			juncY = yPos;
			arrived = heading;
			this.surroundingConditions = new int[4];
			for(int i = 0; i<4; i++)
			{
				this.surroundingConditions[i] = surroundingConditions[i];
			}
		}
	}

	public RobotData_RouteA()
	{
		// Initialize variables
		junctionCounter = 0;
		junctionData = new JunctionData [maxJunctions];
	}

	// Saves log of unencountered junction robot is visiting
	public void recordJunction(int xPos, int yPos, int heading, int[] surroundingConditions)
	{
		junctionData[junctionCounter] = new JunctionData(xPos, yPos, heading, surroundingConditions);
		printJunction();
		junctionCounter++;
	}

	// Prints log of current junction robot is visiting
	public void printJunction()
	{
		String heading = "";

		// Identify the direction robot is coming from & set heading for printing log
		switch(junctionData[junctionCounter].arrived)
		{
			case IRobot.NORTH :	heading = "NORTH";
						break;
			case IRobot.EAST :	heading = "EAST";
						break;
			case IRobot.SOUTH :	heading = "SOUTH";
						break;
			case IRobot.WEST :	heading = "WEST";
						break;
		}

		// Print actual log
		System.out.println("Junction " + (junctionCounter+1)
				+ " (x = " + junctionData[junctionCounter].juncX
				+ ", y = " + junctionData[junctionCounter].juncY
				+ ") heading " + heading);
	}

	// Search whether junction is previously encountered or not
	public int searchJunction(int xPos, int yPos)
	{
		for(int i = 0; i<junctionCounter; i++)	// for each visited junction
		{
			if ( (junctionData[i].juncX == xPos) && (junctionData[i].juncY == yPos) )
			{	// record found
				// return robot's heading when it encountered junction for the first time
				return junctionData[i].arrived;
			}
		}
		return -1;	// junction record for (xPos, yPos) not found, return -1
	}

	// Updates log surroundings of junction(xPos, yPos) with 'value' at 'loc' position
	public void updateJunction(int xPos, int yPos, int loc, int value)
	{
		for(int i = 0; i<junctionCounter; i++)	// for each visited junction
		{
			if ( (junctionData[i].juncX == xPos) && (junctionData[i].juncY == yPos) )
			{
				junctionData[i].surroundingConditions[loc] = value;
				break;
			}
		}
	}

	/* Returns the direction that leads to target (if there is any)
	    Else returns -1
	*/
	public int getOptimalTurn(int xPos, int yPos)
	{
		for(int j = 0; j<junctionCounter; j++)	// for each visited junction
		{
			// If junction record is found for (xPos, yPos)
			if ( (junctionData[j].juncX == xPos) && (junctionData[j].juncY == yPos) )
			{
				for(int i = 0; i<4; i++)	// for all directions (i.e. ahead, right, behind and left)
				{
					// If 1 then right direction; return index of that location
					if (junctionData[j].surroundingConditions[i] == 1)
						return i;
				}
			}
		}
		return -1;
	}
}