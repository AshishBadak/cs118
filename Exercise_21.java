import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_21
{
	private int pollRun = 0;		// Counter for no. of steps in each run
	private RobotData robotData;
	private int explorerMode;	// 1 = explorer mode; 0 = backtracking mode

	public void controlRobot(IRobot robot)
	{
		int direction;

		if ( (robot.getRuns() == 0) && (pollRun == 0))
		{
			// On the first move of the first run of a new maze
			robotData = new RobotData();
			explorerMode = 1;
		}

		if (explorerMode == 1)				// Unecountered pathway available
		{
			direction = exploreControl(robot);
		}
		else						// No unencountered pathway available; backtrack
		{
			direction = backtrackControl(robot);
		}	
				
		robot.face(direction);  		// Face the robot in this direction
		robot.advance();		// Move the robot
		pollRun++;			// Increment for each step
	}

	// Controller for exploring mode
	public int exploreControl(IRobot robot)
	{
		int direction;
		int exits = nonwallExits(robot);

		// Set the 'direction' based on exits available at current position
		switch(exits)
		{
			case 1 : // Only 1 exit; so ROBOT is at DEAD-END
				direction = deadEnd(robot);
				break;
			case 2 : // 2 exits; so ROBOT is at CORRIDOR
				direction = corridor(robot);
				break;
			default : // 3 or 4 exits; so ROBOT is at JUNCTION or CROSS-ROAD
				direction = junctionOrCrossRoad(robot);
				break;
		}
		return direction;
	}

	// Controller for backtracking mode
	public int backtrackControl(IRobot robot)
	{
		int direction;
		int exits = nonwallExits(robot);
		switch(exits)
		{
			case 1 : // Only 1 exit; so ROBOT is at DEAD-END
				direction = deadEnd(robot);
				break;
			case 2 : // 2 exits; so ROBOT is at CORRIDOR
				if (passageExits(robot) != 0)
					explorerMode =1;		// Unvisited passage found; change mode to explore
				direction = corridor(robot);
				break;
			default : // 3 or 4 exits; so ROBOT is at JUNCTION or CROSS-ROAD
				direction = junctionOrCrossRoad(robot);
		}
		return direction;
	}

	// Returns count of available exits at robot's current position
	private int nonwallExits(IRobot robot)
	{
	   	int nonWallExitsCount = 0;

    		// Loop checks no. of non-wall sides to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
    			{
	    			nonWallExitsCount++;
    			}
    		}

    		return nonWallExitsCount;
	}

	// Returns count of non-visited exits at robot's current position
	private int passageExits(IRobot robot)
	{
	   	int passageExitsCount = 0;

    		// Loop checks no. of PASSAGE adjacent to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
    			{
	    			passageExitsCount++;
    			}
    		}
    		return passageExitsCount;
	}

	// Returns count of visits to current position
	private int beenbeforeExits(IRobot robot)
	{
	   	int beenbeforeExitsCount = 0;

    		// Loop checks no. of visited exits adjacent to the robot
    		for (int side = 0; side<4; side++)
    		{
	    		if(robot.look(IRobot.AHEAD+side) == IRobot.BEENBEFORE)
    			{
	    			beenbeforeExitsCount++;
    			}
    		}
    		return beenbeforeExitsCount;
	}

	// Handles the situation when robot is at a dead-end
	private int deadEnd(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;

		if (pollRun == 0)
		{	
			/*
			Dont reverse direction as we are at starting position;
			instead choose only available exit
			*/
			for(int i = 1; i<=3 && ( robot.look(optimalDirection)==IRobot.WALL) ; i++)
			{
				optimalDirection = IRobot.AHEAD+i;
			}
		}
		else
		{	// Turn back;
			explorerMode = 0;
			optimalDirection = IRobot.BEHIND;
		}
		return optimalDirection;
	}

	// Handles the situation when robot is down a corridor
	private int corridor(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;
		
		// Finds exit other than the one robot is coming from
		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
		{
			// Road available; Go AHEAD
			optimalDirection = IRobot.AHEAD;
		}
		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
		{
			// Road available; Turn LEFT
			optimalDirection = IRobot.LEFT;
		}
		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
		{
			// Road available; Turn RIGHT
			optimalDirection = IRobot.RIGHT;
		}
		return optimalDirection;
	}

	// Handles the situation when robot is at a junction or cross-road
	private int junctionOrCrossRoad(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;
		int passageExits = passageExits(robot);
		int visitCount  = beenbeforeExits(robot);
		int randomNum;

		if (visitCount == 1)	// If Junction is visited first-time
		{
			robotData.recordJunction(robot.getLocationX(), robot.getLocationY(), robot.getHeading());
		}

		if (passageExits == 0)
			explorerMode = 0;	// no new pathways; select backtrack mode
		else
			explorerMode = 1;	// Unvisited passage found; change mode to explore

		if (explorerMode == 0)		// Go towards where robot came from at first time
		{
	     		int arrivedFrom = robotData.searchJunction(robot.getLocationX(), robot.getLocationY());
			int goTowards;

			// Reverse arrival direction
			switch(arrivedFrom)
			{
				case IRobot.NORTH :
					goTowards = IRobot.SOUTH ;
					break;
				case IRobot.EAST :
					goTowards = IRobot.WEST;
					break;
				case IRobot.SOUTH :
					goTowards = IRobot.NORTH;
					break;
				default :
					goTowards = IRobot.EAST;
			}
			robot.setHeading(goTowards);
		}
		else
		{
			// One or more PASSAGE available; Select randomly among them
			do
			{
				randomNum = randomNumber(0, 3);
				optimalDirection = IRobot.AHEAD+randomNum;
			}while(robot.look(optimalDirection) != IRobot.PASSAGE);
		}
		
		return optimalDirection;
	}

	// Invoked when Reset button is pressed
	public void reset()
	{
		pollRun = 0;
		explorerMode = 1;
		robotData.resetJunctionCounter();
	}

	// Returns a random number within range of min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}

/**
* This class stores log of visited junctions
*/
class RobotData
{
	private static int maxJunctions = 10000;
	private static int junctionCounter;		// no. of junctions visited
	private static JunctionData junctionData[];

	// This inner-class saves junction-specific data
	private class JunctionData
	{
		private int juncX;				// x co-ordinate of junction
		private int juncY;				// y co-ordinate of junction
		private int arrived;				// direction from which robot arrived to junction

		public JunctionData(int xPos, int yPos, int heading)
		{
			juncX = xPos;
			juncY = yPos;
			arrived = heading;
		}
	}

	public RobotData()
	{
		// Initialize variables
		junctionCounter = 0;
		junctionData = new JunctionData [maxJunctions];
	}

	// Resets visited junction counter to 0 after each run
	public void resetJunctionCounter()
	{
		junctionCounter = 0;
	}

	// Saves log of unencountered junction robot is visiting
	public void recordJunction(int xPos, int yPos, int heading)
	{
		junctionData[junctionCounter] = new JunctionData(xPos, yPos, heading);
		printJunction();
		junctionCounter++;
	}

	// Prints log of current junction robot is visiting
	public void printJunction()
	{
		String heading = "";

		// Identify the direction robot is coming from & set heading for printing log
		switch(junctionData[junctionCounter].arrived)
		{
			case IRobot.NORTH :	heading = "NORTH";
						break;
			case IRobot.EAST :	heading = "EAST";
						break;
			case IRobot.SOUTH :	heading = "SOUTH";
						break;
			case IRobot.WEST :	heading = "WEST";
						break;
		}

		// Print actual log
		System.out.println("Junction " + (junctionCounter+1)
				+ " (x = " + junctionData[junctionCounter].juncX
				+ ", y = " + junctionData[junctionCounter].juncY
				+ ") heading " + heading);
	}

	// Search whether junction is previously encountered or not
	public int searchJunction(int xPos, int yPos)
	{
		for(int i = 0; i<junctionCounter; i++)	// for each visited junction
		{
			if ( (junctionData[i].juncX == xPos) && (junctionData[i].juncY == yPos) )
			{	// record found
				// return robot's heading when it encountered junction for the first time
				return junctionData[i].arrived;
			}
		}
		return -1;	// junction record for (xPos, yPos) not found, return -1
	}
}