import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_13
{
	private int randNum;
	private int direction;
	private byte facingV;
	private byte facingH;

	public void controlRobot(IRobot robot)
	{

		facingV = isTargetNorth(robot);
		facingH = isTargetEast(robot);

		do
		{
			//Select a random number with equal probability.
			randNum = randomNumber(0, 3);
			
			// Convert this to a direction
			switch(randNum)
			{
				case 0:	direction = IRobot.LEFT;
					break;
				case 1:	direction = IRobot.RIGHT;
					break;
				case 2:	direction = IRobot.BEHIND;
					break;
				default:	direction = IRobot.AHEAD;
			}
		}while(robot.look(direction)==IRobot.WALL);//keeps choosing a direction till it finds a non wall one

		// Face the robot in this direction
		robot.face(direction);  

		// Move the robot
		robot.advance();
	}

	// Generates random number within range min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}

	// Checks whether Target is in the North direction or in the South relative to Robot's current position
	private byte isTargetNorth(IRobot robot)
	{
		byte result;

		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same latitude’

		if (robot.getLocationY() > robot.getTargetLocation().y )
		{
			result = 1;
			System.out.println("Target is in the north direction.");
		}
		else if (robot.getLocationY() < robot.getTargetLocation().y )
		{
			result = -1;
			System.out.println("Target is in the south direction.");
		}
		else
		{
			result = 0;
			System.out.println("Target is at same latitude.");
		}

		return result;
	}

	// Checks whether Target is in the East direction or in the West relative to Robot's current position
	private byte isTargetEast(IRobot robot)
	{
		byte result;

		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same latitude’

		if (robot.getLocationX() < robot.getTargetLocation().x )
		{
			result = 1;
			System.out.println("Target is in the east direction.");
		}
		else if (robot.getLocationX() > robot.getTargetLocation().x )
		{
			result = -1;
			System.out.println("Target is in the west direction.");
		}
		else
		{
			result = 0;
			System.out.println("Target is at same langitude.");
		}

		return result;
	}
}
